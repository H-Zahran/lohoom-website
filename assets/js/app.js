jQuery(window).load(function() {
	// $('[data-toggle="popover"]').popover();
	
	$('.header-nav-secondary').affix({
	  offset: {
	    top: function(){
				return (this.top = $('.header-nav-secondary').offset().top)
			}
	  }
	})	

	$('#search-popover').popover({
	'html': true,
	'content': '<form class="navbar-form navbar-left search-popover-form" role="search" action="" method="get">'+
							'<div class="input-group"><input type="text" name="q" value="" class="form-control" placeholder="Search"><span class="input-group-btn"><button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button></span></div>'+
							'</form>',
	});

  jQuery("#name").owlCarousel({
  });
  jQuery("#lohoom-slider").owlCarousel({
    items : 4,
    autoWidth:true,
    itemsCustom : false,
    itemsDesktop : [1199,2],
    itemsDesktopSmall : [980,2],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,2],
    singleItem : false,
    itemsScaleUp : false,
    pagination: false,
    navigation: true,
    navigationText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
  });
});
$('.next-item').click(function(e) {
	e.preventDefault();
	$(".owl-carousel").trigger('prev.owl.carousel');
});
